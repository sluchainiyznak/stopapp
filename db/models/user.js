var path = require('path');
var mongoose = require(path.join(process.cwd(), 'db/connection'));
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: {type: String},
  password: {type: String},
  token: {type: String},
  avatar: {type: String, default: 'avatar.jpg'},
  score: {type: Number, default: 0},
  lives: {type: Number, default: 0}
  });
var User = mongoose.model('User', userSchema);

module.exports = User;