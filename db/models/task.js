var path = require('path');
var mongoose = require(path.join(process.cwd(), 'db/connection'));
var Schema = mongoose.Schema;

var taskSchema = new Schema({
  userId: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User'},
  createdAt: {type: Date, required: true, default: Date.now},
  text: {type: String, required: true},
  label: {type: String},
  level: {type: Number, default: 0},
  comments: [{
    createdAt: {type: Date, required: true, default: Date.now},
    text: {type: String, required: true},
    mark: {type: Boolean}
  }]
});

var Task = mongoose.model('Task', taskSchema);

module.exports = Task;