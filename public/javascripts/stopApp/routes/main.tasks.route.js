'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main.tasks', {
        url: '/tasks',
        views: {
          'dashboard@': {
            templateUrl: '/templates/tasks/list.html',
            controller: 'TasksController',
            controllerAs: 'tasksCtrl',
            resolve: {
              tasks: ['tasksFactory', function (tasksFactory) {
                var res = tasksFactory.query().$promise;
                return res;
              }]
            }
          }
        }
      });
  }
})();