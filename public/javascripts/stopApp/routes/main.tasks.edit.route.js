'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main.tasks.edit', {
        url: '/:id/edit',
        params: {id: {squash: false}},
        views: {
          'dashboard@': {
            templateUrl: '/templates/tasks/edit.html',
            controller: 'EditTaskController',
            controllerAs: 'taskCtrl',
            resolve: {
              task: ['tasksFactory', '$stateParams', function (tasksFactory, $stateParams) {
                if ($stateParams.id) {
                  return tasksFactory.get({id: $stateParams.id}).$promise;
                } else {
                  return {};
                }
              }]
            }
          }
        }
      });
  }
})();