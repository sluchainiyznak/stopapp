'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main.tasks.view', {
        url: '/:id',
        params: {id: {squash: false}},
        views: {
          'dashboard@': {
            templateUrl: '/templates/tasks/view.html',
            controller: 'ViewTaskController',
            controllerAs: 'taskCtrl',
            resolve: {
              task: ['tasksFactory', '$stateParams', function (tasksFactory, $stateParams) {
                if ($stateParams.id) {
                  return tasksFactory.get({id: $stateParams.id}).$promise;
                } else {
                  return {};
                }
              }],
              comments: ['task', 'commentsFactory', function (task, commentsFactory) {
                if (task._id) {
                  return commentsFactory.query({id: task._id}).$promise;
                } else {
                  return [];
                }
              }]
            }
          }
        }
      });
  }
})();