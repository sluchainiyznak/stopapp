'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('register', {
        url: '/register',
        views: {
          'main@': {
            templateUrl: '/templates/auth/register.html',
            controller: 'AuthController',
            controllerAs: 'authCtrl'
          }
        }
      })
      .state('login', {
        url: '/login',
        views: {
          'main@': {
            templateUrl: '/templates/auth/login.html',
            controller: 'AuthController',
            controllerAs: 'authCtrl'
          }
        }
      });
  }
})();