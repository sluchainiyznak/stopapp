'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main.user-view', {
        url: '/user/:username',
        params: {id: {value: 'self'}},
        views: {
          'dashboard@': {
            templateUrl: '/templates/user/view.html',
            controller: 'ProfileViewController',
            controllerAs: 'profileViewCtrl'
          },
          'sidebar@': {
            templateUrl: '/templates/sidebar.html',
            controller: 'SidebarController',
            controllerAs: 'sidebarCtrl'
          }
        },
        resolve: {
          profile: ['userFactory', '$stateParams', function (userFactory, $stateParams) {
            return userFactory.fetch({username: $stateParams.username});
          }]
        }
      });
  }
})();