'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main.user-edit', {
        url: '/edit-profile',
        params: {id: {value: 'self'}},
        views: {
          'dashboard@': {
            templateUrl: '/templates/user/edit.html',
            controller: 'ProfileEditController',
            controllerAs: 'profileCtrl'
          }
        },
        resolve: {
          profile: ['userFactory', function (userFactory) {
            return userFactory.fetch({username: 'self'});
          }]
        }
      });
  }
})();