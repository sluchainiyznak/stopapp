'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main', {
        abstract: true,
        views: {
          'sidebar@': {
            templateUrl: '/templates/sidebar.html',
            controller: 'SidebarController',
            controllerAs: 'sidebarCtrl',
            resolve: {
              profile: ['userFactory', function (userFactory) {
                var res = userFactory.fetch({username: 'self'}).$promise;
                return res;
              }],
              stats: ['statsFactory', function (statsFactory) {
                var res = statsFactory.tasks().query();

                return res;
              }]
            }
          }
        }
      });
  }
})();