'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$stateProvider'];
  function config($stateProvider) {
    $stateProvider
      .state('main.about', {
        url: '/about',
        views: {
          'dashboard@': {
            templateUrl: '/templates/about.html',
            controller: 'AboutController',
            controllerAs: 'aboutCtrl'
          }
        }
      });
  }
})();