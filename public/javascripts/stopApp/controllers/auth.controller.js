'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('AuthController', AuthController);

  AuthController.$inject = ['$state', 'AuthFactory', '$scope'];
  function AuthController($state, AuthFactory, $scope) {
    var vm = this;
    vm.register = register;
    vm.login = login;
    vm.logout = logout;
    vm.user = {username: '', password: ''};

    function register(user) {
      AuthFactory.register(user.username, user.password, function (obj) {
        if (obj) user.hasErrors = true;
        else {
          $scope.$emit('app:login');
          $state.go('main.dashboard');
        }
      });
    }

    function login(user) {
      AuthFactory.login(user.username, user.password, function (obj) {
        if (obj) user.hasErrors = true;
        else {
          $scope.$emit('app:login');
          $state.go('main.dashboard');
        }
      });
    }

    function logout() {
      AuthFactory.logout(function () {
        $state.reload();
      });
    }
  }
})();