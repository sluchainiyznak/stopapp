'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('CommentController', CommentController);

  CommentController.$inject = ['commentsFactory', 'AlertsStore'];
  function CommentController(commentsFactory, AlertsStore) {
    var vm = this;
    vm.commentText = '';
    vm.addComment = addComment;

    function addComment(task) {
      commentsFactory.save({id: task._id}, {text: vm.commentText},
        function (res) {
          vm.commentText = null;
          task.comments = commentsFactory.query({id: task._id});
          AlertsStore.addAlert('Comment successful added!', AlertsStore.alertEvents.success);
        });
    }
  }
})();