'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('AboutController', AboutController);

  AboutController.$inject = ['$modal'];
  function AboutController($modal) {
    var vm = this;
    vm.open = function (size) {
      $modal.open({templateUrl: '/templates/about.html'});
    };

  }
})();