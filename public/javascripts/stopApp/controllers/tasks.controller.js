'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('TasksController', TasksController);

  TasksController.$inject = ['tasks', 'taskHelper'];
  function TasksController(tasks, taskHelper) {
    var vm = this;
    vm.tableData = tasks;
    vm.levelHelper = taskHelper.getLevelLabel;
  }
})();