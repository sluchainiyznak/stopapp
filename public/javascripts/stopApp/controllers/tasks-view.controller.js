'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('ViewTaskController', ViewTaskController);

  ViewTaskController.$inject = ['task', 'comments', 'taskHelper'];
  function ViewTaskController(task, comments, taskHelper) {
    var vm = this;
    vm.task = task;
    task.comments = comments;
  }
})();