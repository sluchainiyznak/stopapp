'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('SidebarController', SidebarController);

  SidebarController.$inject = ['profile', 'stats', 'taskHelper', 'Upload', '$state', '$rootScope', 'AlertsStore'];
  function SidebarController(profile, stats, taskHelper, Upload, $state, $rootScope, AlertsStore) {
    var vm = this;
    vm.stats = {};
    vm.user = profile;
    vm.newAvatar = undefined;
    vm.stats.tasks = stats;
    vm.getLevelLabel = taskHelper.getLevelLabel;
    vm.owner = $rootScope.user._id === vm.user._id;

    vm.upload = upload;
    vm.getTasksCount = getTasksCount;

    function upload(files) {
      if (files && files.length) {
        var file = files[0];
        Upload.upload({
          url: 'api/users/profile/avatar',
          file: file
        }).success(function (data, status, headers, config) {
          AlertsStore.addAlert('User avatar has been updated', AlertsStore.alertEvents.success);
          $state.reload();
        });
      }
    }

    function getTasksCount(taskGroupedByStatus) {
      var count = 0;
      angular.forEach(taskGroupedByStatus, function (value) {
        count += value.total;
      });

      return count;
    }
  }
})();