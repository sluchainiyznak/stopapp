'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('EditTaskController', EditTaskController);

  EditTaskController.$inject = ['task', '$state', 'tasksFactory', 'AlertsStore', 'taskHelper'];
  function EditTaskController(task, $state, tasksFactory, AlertsStore, taskHelper) {
    var vm = this;
    vm.levels = taskHelper.getLevelLabels();
    vm.task = {
      text: {
        value: task.text,
        errors: null
      },
      level: {
        value: task.level,
        errors: null
      },
      label: {
        value: task.label,
        errors: null
      }
    };
    vm.save = save;

    function save(data) {
      var model = {
        text: data.text.value,
        level: data.level.value,
        label: data.label.value
      };
      if (task._id) {
        tasksFactory.update({id: task._id}, model, successCallback, errorCallback);
      } else {
        tasksFactory.save(model, successCallback, errorCallback);
      }
    }

    function successCallback(res) {
      $state.go('main.tasks.view', {id: res._id}, {reload: true});
    }

    function errorCallback(err) {
      if (err.data.validationErrors) {
        angular.forEach(err.data.validationErrors, function(item) {
          vm.task[item.param].error = item.msg;
        });
      } else {
        AlertsStore.addAlert('Something wrong', AlertsStore.alertEvents.danger);
      }
    }
  }
})();