'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('ProfileViewController', ProfileViewController);

  ProfileViewController.$inject = ['profile', '$state'];
  function ProfileViewController(resolvedProfile, $state) {
    var vm = this;
    vm.profile = resolvedProfile;
  }
})();