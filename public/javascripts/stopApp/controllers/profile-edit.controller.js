'use strict';
(function () {
  angular
    .module('stopApp')
    .controller('ProfileEditController', ProfileEditController);

  ProfileEditController.$inject = ['profile', '$state', 'userFactory', 'AlertsStore'];
  function ProfileEditController(profile, $state, userFactory, AlertsStore) {
    var vm = this;
    vm.user = {
      username: {
        value: profile.username,
        errors: null
      }
    };
    vm.save = save;

    function save(data) {
      userFactory.update({username: data.username.value},
        function (res) {
          $state.go('main.dashboard', null, {reload: true});
        }, function (err) {
          if (err.data.validationErrors) {
            angular.forEach(err.data.validationErrors, function(item) {
              vm.user[item.param].error = item.msg;
            });
            vm.user.hasErrors = true;
          } else {
            AlertsStore.addAlert('Something is wrong!', AlertsStore.alertEvents.danger);
          }

        });
    }
  }
})();