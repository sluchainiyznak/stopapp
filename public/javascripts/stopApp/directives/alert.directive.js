'use strict';
(function () {
  angular
    .module('stopApp')
    .directive('stopAppAlerts', stopAppAlertFn);

  function stopAppAlertFn() {
    return {
      restrict: 'A',
      replace: false,
      templateUrl: 'templates/directives/alerts.html',
      controller: AlertsController,
      controllerAs: 'alertsCtrl'
    };
  }

  AlertsController.$inject = ['AlertsStore', '$rootScope'];
  function AlertsController(AlertsStore, $rootScope) {
    var vm = this;
    vm.closeAlert = closeAlertFn;
    vm.refresh = refreshFn;
    vm.refresh();

    $rootScope.$on('$stateChangeSuccess', refreshFn);

    function closeAlertFn(alert) {
      AlertsStore.removeAlert(alert);
      vm.refresh();
    }

    function refreshFn() {
      vm.alerts = AlertsStore.getAll();
    }
  }
})();