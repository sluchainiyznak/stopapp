'use strict';
(function () {
  angular
    .module('stopApp')
    .directive('authLink', authLink);

  authLink.$inject = [];
  function authLink() {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: 'templates/directives/auth-link.html',
      controller: AuthLinkController,
      controllerAs: 'authLinkCtrl'
    };
  }

  AuthLinkController.$inject = ['$rootScope', 'AuthFactory'];
  function AuthLinkController($rootScope, AuthFactory) {
    var vm = this;
    vm.isLoggedIn = !AuthFactory.isGuest();
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      vm.isLoggedIn = !AuthFactory.isGuest();
    });
  }
})();