'use strict';
(function () {
  angular.module('stopApp').factory('tasksFactory', tasksFactory);

  tasksFactory.$inject = ['$resource', 'urlHelper'];
  function tasksFactory($resource, urlHelper) {
    var url = urlHelper.getApiUrl() + '/tasks';

    return $resource(url + '/:id', {id: '@id'}, {
      update: {
        method: 'PUT'
      }
    });
  }
})();