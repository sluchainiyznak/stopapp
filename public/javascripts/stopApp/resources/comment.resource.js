'use strict';
(function () {
  angular.module('stopApp').factory('commentsFactory', commentsFactory);

  commentsFactory.$inject = ['$resource', 'urlHelper'];
  function commentsFactory($resource, urlHelper) {
    var url = urlHelper.getApiUrl() + '/tasks/:id/comments';

    return $resource(url, null);
  }
})();