'use strict';
(function () {
  angular.module('stopApp').factory('userFactory', userFactory);

  userFactory.$inject = ['$resource', 'urlHelper'];
  function userFactory($resource, urlHelper) {
    var url = urlHelper.getApiUrl() + '/users';

    return $resource(urlHelper.getApiUrl(), null, {
      register: {
        url: urlHelper.getApiUrl() + '/register',
        method: 'POST'
      },
      login: {
        url: urlHelper.getApiUrl() + '/login',
        method: 'POST'
      },
      update: {
        url: url + '/profile',
        method: 'PUT'
      },
      fetch: {
        url: url + '/profile/:username',
        method: 'GET',
        params: {
          username: '@username'
        },
        isArray: false
      },
      list: {
        url: url,
        method: 'GET',
        isArray: true
      }
    });
  }
})();