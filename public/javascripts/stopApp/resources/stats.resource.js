'use strict';
(function () {
  angular.module('stopApp').factory('statsFactory', statsFactory);

  statsFactory.$inject = ['$resource', 'urlHelper'];
  function statsFactory($resource, urlHelper) {
    var url = urlHelper.getApiUrl() + '/stats';

    return {
      tasks: function () {
        return $resource(url + '/tasks');
      }
    }
  }
})();