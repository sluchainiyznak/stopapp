'use strict';
(function () {
  angular
    .module('stopApp', [
      'ui.router',
      'ngAnimate',
      'ngResource',
      'LocalStorageModule',
      'angularSpinner',
      'ngFileUpload',
      'ui.bootstrap'
    ]
  )
    .config(config)
    .run(run);

  config.$inject = ['$urlRouterProvider', '$locationProvider'];
  function config($urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
  }

  run.$inject = ['$rootScope', 'userFactory'];
  function run($rootScope, userFactory) {
    $rootScope.$on('app:login', function() {
      userFactory.fetch({username: 'self'}).$promise.then(function(user) {
        $rootScope.user = user;
      });
    });
    $rootScope.$emit('app:login');
  }
})();