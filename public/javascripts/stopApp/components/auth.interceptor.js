'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config);

  config.$inject = ['$httpProvider'];
  function config($httpProvider) {
    $httpProvider.interceptors.push(authInterceptor);
    authInterceptor.$inject = ['$injector', '$q'];
    function authInterceptor($injector, $q) {
      return {
        request: function (config) {
          var authFactory = $injector.get('AuthFactory');
          if (!authFactory.isGuest()) {
            config.headers['Authorization'] = 'Bearer ' + authFactory.getAuthKey();
          }
          return config;
        },
        responseError: function (response) {
          if (response.status === 401 || response.status === 403) {
            return $injector.get('$state').go('login');
          }
          return $q.reject(response);
        }
      };
    }
  }
})();