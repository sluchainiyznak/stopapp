'use strict';
(function () {
  angular
    .module('stopApp')
    .config(config)
    .run(run);

  config.$inject = ['usSpinnerConfigProvider'];
  function config(usSpinnerConfigProvider) {

  }

  run.$inject = ['$rootScope', 'usSpinnerService'];
  function run($rootScope, usSpinnerService) {
    $rootScope.$on('$stateChangeStart', startSpinner);
    $rootScope.$on('$stateChangeSuccess', stopSpinner);
    $rootScope.$on('$stateChangeError', stopSpinner);

    function startSpinner() {
      usSpinnerService.spin('spinner-1');
    }

    function stopSpinner() {
      usSpinnerService.stop('spinner-1');
    }
  }
})();