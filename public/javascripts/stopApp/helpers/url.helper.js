'use strict';
(function () {
  angular.module('stopApp').factory('urlHelper', urlHelper);

  urlHelper.$inject = [];
  function urlHelper() {
    var vm = this;
    vm.apiUrl = '/api';
    return {
      getApiUrl: function () {
        return vm.apiUrl
      }
    };
  }
})();