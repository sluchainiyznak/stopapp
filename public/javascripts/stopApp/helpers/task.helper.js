'use strict';
(function () {
  angular.module('stopApp').factory('taskHelper', taskHelper);

  taskHelper.$inject = ['$filter'];
  function taskHelper($filter) {
    var vm = this;
    vm.levels = [
      {id: 0, title: 'info'},
      {id: 1, title: 'warning'},
      {id: 2, title: 'danger'},
      {id: 3, title: 'success'}
    ];

    return {
      levels: vm.levels,
      getLevelLabels: getLevelLabels,
      getLevelLabel: getLevelLabel
    };

    function getLevelLabels() {
      return angular.forEach(vm.levels, function(value, key) {
        return {value: key};
      });
    }

    function getLevelLabel(level) {
      var matches = $filter('filter')(vm.levels, {id: level});
      return matches[0].title;
    }
  }
})();