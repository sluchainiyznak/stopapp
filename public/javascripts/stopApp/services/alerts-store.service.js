'use strict';
(function () {
  angular.module('stopApp').factory('AlertsStore', serviceFn);

  serviceFn.$inject = ['$filter'];
  function serviceFn($filter) {
    var vm = this;
    vm.alertEvents = {
      info: 'info',
      warning: 'warning',
      success: 'success',
      danger: 'danger'
    };
    vm.alerts = [];

    return new function () {
      var _this = this;
      _this.alertEvents = vm.alertEvents;
      _this.getAll = getAllFn;
      _this.addAlert = addAlertFn;
      _this.removeAlert = removeAlertFn;
    };

    function getAllFn() {
      return vm.alerts;
    }

    function addAlertFn(content, eventType) {
      var alertEvent = eventType || vm.alertEvents.info;
      vm.alerts.push({alertEvent: alertEvent, content: content});
    }

    function removeAlertFn(alert) {
      vm.alerts = $filter('filter')(vm.alerts, {$$hashKey: '!' + alert.$$hashKey});
    }
  }
})();