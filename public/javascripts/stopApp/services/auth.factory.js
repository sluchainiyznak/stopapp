'use strict';
(function () {
  angular
    .module('stopApp')
    .factory('AuthFactory', AuthFactory);

  AuthFactory.$inject = ['localStorageService', 'userFactory'];
  function AuthFactory(localStorageService, userFactory) {
    var vm = this;
    vm.authKey = 'auth_key';

    return {
      isGuest: isGuestFunc,
      register: registerFunc,
      login: loginFunc,
      logout: logoutFunc,
      getAuthKey: getAuthKeyFunc,
      getUser: getUser
    };


    function isGuestFunc() {
      return !localStorageService.get(vm.authKey);
    }

    function registerFunc(username, password, cb) {
      return userFactory
        .register({username: username, password: password})
        .$promise.then(function (data) {
          localStorageService.set(vm.authKey, data.token);
          cb();
        }, function(err) {
          if (err.data.validationErrors) {
            cb({hasErrors: true});
          }
        });
    }

    function loginFunc(username, password, cb) {
      return userFactory
        .login({username: username, password: password})
        .$promise.then(function (data) {
          localStorageService.set(vm.authKey, data.token);
          cb();
        }, function(err) {
          if (err.data.validationErrors) {
            cb({hasErrors: true});
          }
        });
    }

    function logoutFunc(cb) {
      localStorageService.remove(vm.authKey);
      return cb();
    }

    function getAuthKeyFunc() {
      return localStorageService.get(vm.authKey);
    }

    function getUser() {
      return userFactory.fetch().$promise;
    }
  }
})();