var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var Task = require(path.join(process.cwd(), 'db/models/task'));


router.get('/tasks', passport.authenticate('bearer', {session: false}), function (req, res, next) {
  Task.find(
    {userId: req.user._id},
    {_id: true, text: true, createdAt: true, userId: true, level: true, label: true, comments: true},
    {
      sort:{
        createdAt: -1
      }
    },
    function (err, data) {
      if (err) return next(err);
      return res.json(data);
    });

});

module.exports = router;