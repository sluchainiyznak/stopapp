var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var bodyParser = require('body-parser');
var Task = require(path.join(process.cwd(), 'db/models/task'));
var validator = require('express-validator');

router.post('/tasks', bodyParser.json(), validator(), function(req, res, next) {
  req.checkBody('text', 'Поле обязательно для заполнения').notEmpty();
  req.checkBody('level', 'Поле обязательно для заполнения').optional().isInt();

  var validationErrors = req.validationErrors();
  if (validationErrors) {
    var err = new Error('validationErrors');
    err.errors = validationErrors;

    return next(err);
  }
  return next();
});

router.post('/tasks', passport.authenticate('bearer', {session: false}),
  function (req, res, next) {
    var small = new Task({
      text: req.body.text,
      label: req.body.label,
      level: req.body.level,
      userId: req.user._id
    });
    small.save(function(err, task) {
      if (err) return next(err);
      return res.json(task);
    });
  }
);

module.exports = router;