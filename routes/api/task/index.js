var express = require('express');
var router = express.Router();

router.use(require('./one'));
router.use(require('./list'));
router.use(require('./create'));
router.use(require('./update'));

module.exports = router;