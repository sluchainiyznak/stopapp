var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var path = require('path');
var User = require(path.join(process.cwd(), 'db/models/user'));
var passport = require('passport');
var multipart = require('connect-multiparty');
var mv = require('mv');
var fs = require('fs');

router.post(
  '/profile/avatar',
  passport.authenticate('bearer', {session: false}),
  multipart(),
  function (req, res, next) {
    if (req.files.file) {
      var newFileName = crypto.randomBytes(20).toString('hex') + path.extname(req.files.file.path);

      User.findOne({username: req.user.username}, function (err, user) {
        if (err) next(err);
        fs.exists(path.join(process.cwd(), 'public/images', user.avatar), function (exists) {
          if (exists) {
            fs.unlinkSync(path.join(process.cwd(), 'public/images', user.avatar));
          }
          mv(req.files.file.path, path.join(process.cwd(), 'public/images', newFileName), function (err) {
            if (err) throw err;
            user.avatar = newFileName;
            user.save(function (err) {
              if (err) return next(err);
              res.send(user);
            });
          });
        });

      });
    }
  }
);

module.exports = router;