var express = require('express');
var router = express.Router();

router.use(require('./profile'));
router.use(require('./update'));
router.use(require('./update-avatar'));
router.use(require('./list'));

module.exports = router;