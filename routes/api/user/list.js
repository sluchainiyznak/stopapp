var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var User = require(path.join(process.cwd(), 'db/models/user'));


router.get('/', passport.authenticate('bearer', {session: false}), function (req, res, next) {
  User.find({_id: {$ne: req.user._id}}, {
    _id: true,
    username: true,
    score: true,
    lives: true
  }, function (err, data) {
    if (err) next(err);
    if (data) {
      res.end(JSON.stringify(data));
    } else {
      res.status(404);
      next(new Error('not found'))
    }
  });
});

module.exports = router;