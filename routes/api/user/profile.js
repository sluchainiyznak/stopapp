var express = require('express');
var router = express.Router();
var passport = require('passport');
var path = require('path');
var mongoose = require('mongoose');
var User = require(path.join(process.cwd(), 'db/models/user'));

router.get('/profile/:username', passport.authenticate('bearer', {session: false}), function (req, res, next) {
  if (req.params.username === 'self') {
    req.params.username = req.user.username;
  }
  User.findOne({username: req.params.username}, {
    username: true,
    score: true,
    lives: true,
    avatar: true
  }, function (err, data) {
    if (err) next(err);
    if (data) {
      res.json(data);
    } else {
      res.status(404);
      res.end();
    }
  });
});

module.exports = router;