var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var bodyParser = require('body-parser');
var User = require(path.join(process.cwd(), 'db/models/user'));
var validator = require('express-validator');

router.use(validator({
  customValidators: {
    unique: function(param, exist) {
      return exist === false;
    }
  }
}));

router.put('/profile', passport.authenticate('bearer', {session: false}), bodyParser.json(),
  function (req, res, next) {
    req.checkBody('username', 'Поле обязательно для заполнения').notEmpty();
    userExist(req.body.username, function (err, exist) {
      if (err) return next(err);
      req.checkBody('username', 'Пользователь уже существует').unique(exist);

      var validationErrors = req.validationErrors();

      if (validationErrors) {
        var localErr = new Error('validationErrors');
        localErr.errors = validationErrors;

        return next(localErr);
      } else {
        updateUser(req.user.username, req.body.username, function (err, user) {
          if (err) return next(err);

          return res.json(user);
        });
      }
    });
  }
);

function userExist(username, cb) {
  User.count({username: username}, function (err, count) {
    if (err) return cb(err);

    return cb(null, count > 0);
  });
}

function updateUser(username, newUsername, cb) {
  User.findOneAndUpdate({username: username}, {$set: {username: newUsername}}, {select: {username: true}}, function (err, user) {
    if (err) return cb(err);

    user.save(function (err) {
      if (err) return cb(err);

      return cb(null, user);
    });
  });
}

module.exports = router;