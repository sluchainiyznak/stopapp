var express = require('express');
var router = express.Router();

router.use(require('./create'));
router.use(require('./list'));

module.exports = router;