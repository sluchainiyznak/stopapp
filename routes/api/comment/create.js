var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var bodyParser = require('body-parser');
var Task = require(path.join(process.cwd(), 'db/models/task'));
var validator = require('express-validator');

router.post('/tasks/:id/comments', bodyParser.json(), validator(), function(req, res, next) {
  req.checkBody('text', 'Поле обязательно для заполнения').notEmpty();

  var validationErrors = req.validationErrors();
  if (validationErrors) {
    var err = new Error('validationErrors');
    err.errors = validationErrors;

    return next(err);
  }
  return next();
});

router.post('/tasks/:id/comments', passport.authenticate('bearer', {session: false}),
  function (req, res, next) {
    Task.findOneAndUpdate(
      {_id: req.params.id, userId: req.user._id},
      {$push: {comments: {
        text: req.body.text,
        mark: true
      }}},
      {select: {comments: true}},
      function (err, task) {
        if (err) return next(err);
        return res.json(task);
      }
    );
  }
);

module.exports = router;