var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var Task = require(path.join(process.cwd(), 'db/models/task'));


router.get('/tasks/:id/comments', passport.authenticate('bearer', {session: false}), function (req, res, next) {
  Task.findOne(
    {_id: req.params.id, userId: req.user._id},
    {comments: true},
    function (err, data) {
      if (err) return next(err);
      return res.json(data.comments);
    });

});

module.exports = router;