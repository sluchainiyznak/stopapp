var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
var Task = require(path.join(process.cwd(), 'db/models/task'));


router.get('/tasks', passport.authenticate('bearer', {session: false}), function (req, res, next) {
  Task.aggregate([
    {$match: {userId: req.user._id}},
    {$group: {_id: '$level', count: {$sum: 1}}},
    {$project : {status : '$_id', total : '$count', _id : 0}},
    {$sort: {status: 1}}
  ], function (err, result) {
    if (err) {
      next(err);
    } else {
      res.json(result);
    }
  });

});

module.exports = router;