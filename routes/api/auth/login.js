var express = require('express');
var router = express.Router();
var path = require('path');
var bodyParser = require('body-parser');
var hash = require('password-hash');
var User = require(path.join(process.cwd(), 'db/models/user'));
var validator = require('express-validator');

router.use(validator({}));

router.post('/login', bodyParser.urlencoded({extended: false}), bodyParser.json(), function (req, res, next) {
  var body = req.body;
  req.checkBody('username', 'Поле обязательно для заполнения').notEmpty();
  req.checkBody('password', 'Поле обязательно для заполнения').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    var err = new Error('validationErrors');
    err.errors = errors;
    next(err);
  } else {
    User.findOne({username: body.username}, function (err, user) {
      if (user && hash.verify(body.password, user.password)) {
        res.json({
          _id: user._id,
          username: user.username,
          password: user.password,
          token: user.token
        }).end();
      } else {
        res.status(404);
        next(new Error('Required username and password fields'));
      }
    });
  }

});

module.exports = router;