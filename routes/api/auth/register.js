var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var hash = require('password-hash');
var path = require('path');
var crypto = require('crypto');
var User = require(path.join(process.cwd(), 'db/models/user'));
var validator = require('express-validator');

router.use(validator({}));

router.post('/register', bodyParser.urlencoded({extended: false}), bodyParser.json(), function (req, res, next) {
  var body = req.body;
  req.checkBody('username', 'Поле обязательно для заполнения').notEmpty();
  req.checkBody('password', 'Поле обязательно для заполнения').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    next(new Error({validationErrors: errors}));
    return;
  }

  User.count({username: body.username}, function (err, count) {
    if (err) next(err);
    if (count > 0) {
      res.status(500);
      res.end('User already exist');
    } else {
      var newUser = new User({
        username: body.username,
        password: hash.generate(body.password),
        token: crypto.randomBytes(20).toString('hex')
      });
      newUser.save(function (err, user) {
        if (err) next(err);
        res.json({
          _id: user._id,
          username: user.username,
          password: user.password,
          token: user.token
        });
      });
    }
  });
});

module.exports = router;