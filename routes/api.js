var express = require('express');
var router = express.Router();
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var path = require('path');

var User = require(path.join(process.cwd(), 'db/models/user'));

passport.use(new BearerStrategy({},
  function (token, done) {
    process.nextTick(function () {
      User.findOne({token: token}, function (err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false);
        }
        return done(null, user);
      })
    });
  }
));

router.use(passport.initialize());
router.use(function (req, res, next) {
  res.responseType = 'json';
  next();
});

router.use(require('./api/auth'));
router.use('/users', require('./api/user'));
router.use(require('./api/comment'));
router.use(require('./api/task'));
router.use('/stats', require('./api/stats'));


router.use(function (err, req, res, next) {
  res.status(400).json({validationErrors: err.errors});
});

module.exports = router;
