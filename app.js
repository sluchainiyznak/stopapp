var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();

app.use('/api', require(path.join(__dirname, 'routes', 'api')));

app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res) {
  res.end(fs.readFileSync(path.join(__dirname, 'public', 'index.html')));
});

module.exports = app;
