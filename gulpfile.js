var gulp = require('gulp');
var gulpMinifyJs = require('gulp-uglify');
var gulpMinifyHtml = require('gulp-minify-html');
var gulpMinifyCss = require('gulp-minify-css');
var gulpConcat = require('gulp-concat');
var bowerMin = require('bower-min');
var merge2 = require('merge2');
var bowerMinJavaScriptFiles = bowerMin('js', 'min.js');

var srcHtml = ['public/templates/*.html', 'public/templates/*/*.html'];
var srcJs = ['public/javascripts/stopApp/*.js', 'public/javascripts/stopApp/*/*.js'];
var srcCss = 'public/stylesheets/*.css';

gulp.task('compressHtml', function () {
  return gulp
    .src(srcHtml)
    .pipe(gulpMinifyHtml())
    .pipe(gulp.dest('public/dist/templates'));
});

gulp.task('compressJs', function () {
  return gulp
    .src(srcJs)
    .pipe(gulpMinifyJs())
    .pipe(gulpConcat('index.js'))
    .pipe(gulp.dest('public/dist'));
});

gulp.task('compressBowerJs', function () {
  return merge2(
    gulp.src(bowerMinJavaScriptFiles.minified),
    gulp.src(bowerMinJavaScriptFiles.minifiedNotFound)
      .pipe(gulpConcat('tmp.min.js'))
      .pipe(gulpMinifyJs())
  )
    .pipe(gulpConcat('vendor-scripts.min.js'))
    .pipe(gulp.dest('public/dist'))
});

gulp.task('compressCss', function () {
  return gulp
    .src(srcCss)
    .pipe(gulpMinifyCss())
    .pipe(gulpConcat('index.css'))
    .pipe(gulp.dest('public/dist'))
});

gulp.task('watch', function () {
  gulp.watch(srcHtml, function () {
    gulp.start('compressHtml');
  });

  gulp.watch(srcJs, function () {
    gulp.start('compressJs');
  });

  gulp.watch([bowerMinJavaScriptFiles.normal, bowerMinJavaScriptFiles.minifiedNotFound, bowerMinJavaScriptFiles.minified], function () {
    gulp.start('compressBowerJs');
  });

  gulp.watch(srcCss, function () {
    gulp.start('compressCss');
  });
  gulp.start('build');
});

gulp.task('build', function () {
  gulp.start('compressHtml');
  gulp.start('compressCss');
  gulp.start('compressJs');
  gulp.start('compressBowerJs');
});